<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${homeworkName}</title>
</head>
<body>

<h1>${homeworkName}</h1>

<#list chapterExerciseList as chapter>
    <h2>${chapter.chapterName}</h2>

    <#list chapter.exerciseList as question>
        <div>
            <p>${question.exerciseText}</p>
        </div>
        <div style="margin-bottom: 100px">
        </div>
    </#list>
</#list>

</body>
</html>
