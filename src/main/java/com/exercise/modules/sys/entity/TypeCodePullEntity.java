package com.exercise.modules.sys.entity;

import lombok.Data;

/**
 * @ClassName: TypeCodePullEntity
 * @Description:
 * @Author: raindrops
 * @Date: 2023/1/17 15:53
 */
@Data
public class TypeCodePullEntity {
    private Integer id;
    private String code;
    private String name;
}
