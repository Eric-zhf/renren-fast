package com.exercise.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 状态码
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-01-31 15:16:05
 */
@Data
@TableName("type_code")
public class TypeCodeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	private Integer parentId;
	private Integer gradeId;
	/**
	 * 
	 */
	private String name;
	/**
	 * 1 上线 -1 下线
	 */
	private Integer status;
	/**
	 * 1 上线 -1 下线
	 */
	private Integer type;

	@TableField(exist = false)
	private List<TypeCodeEntity> subList;

	private Integer weight;

}
