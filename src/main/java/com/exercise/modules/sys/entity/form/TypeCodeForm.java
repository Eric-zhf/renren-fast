package com.exercise.modules.sys.entity.form;

import com.exercise.common.utils.PageInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName: TypeCodeForm
 * @Description:
 * @Author: raindrops
 * @Date: 2023/1/31 15:49
 */
@Data
@ApiModel("服务分类")
public class TypeCodeForm extends PageInfo {
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("类型，1知识点管理，2数据字典")
    private Integer type;
    private Integer parentId;
    private Integer gradeId;
}
