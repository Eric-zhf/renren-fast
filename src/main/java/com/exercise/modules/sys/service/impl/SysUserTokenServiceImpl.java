/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.exercise.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exercise.common.utils.DateUtils;
import com.exercise.modules.structure.dao.GradeClassDao;
import com.exercise.modules.structure.dao.SchoolDao;
import com.exercise.modules.structure.entity.GradeClassEntity;
import com.exercise.modules.structure.entity.SchoolEntity;
import com.exercise.modules.sys.dao.SysUserTokenDao;
import com.exercise.modules.sys.entity.SysUserEntity;
import com.exercise.modules.sys.entity.SysUserTokenEntity;
import com.exercise.modules.sys.oauth2.TokenGenerator;
import com.exercise.modules.sys.service.SysUserTokenService;
import com.exercise.common.utils.R;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Service("sysUserTokenService")
public class SysUserTokenServiceImpl extends ServiceImpl<SysUserTokenDao, SysUserTokenEntity> implements SysUserTokenService {
	//12小时后过期
	private final static int EXPIRE = 3600 * 12;

	private final GradeClassDao gradeClassDao;
	private final SchoolDao schoolDao;

	public SysUserTokenServiceImpl(GradeClassDao gradeClassDao, SchoolDao schoolDao) {
		this.gradeClassDao = gradeClassDao;
		this.schoolDao = schoolDao;
	}


	@Override
	public R createToken(SysUserEntity user) {
		long userId = user.getUserId();
		//生成一个token
		String token = TokenGenerator.generateValue();

		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

		//判断是否生成过token
		SysUserTokenEntity tokenEntity = this.getById(userId);
		if(tokenEntity == null){
			tokenEntity = new SysUserTokenEntity();
			tokenEntity.setUserId(userId);
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//保存token
			this.save(tokenEntity);
		}else{
//			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);
			token = tokenEntity.getToken();
			//更新token
			this.updateById(tokenEntity);
		}
		Map<String,Object> data = new HashMap<>();
		String roleName = "Sys_Admin";
		int classId = 0;
		SchoolEntity schoolEntity = schoolDao.selectOne(new MPJLambdaWrapper<SchoolEntity>().eq(SchoolEntity::getPrincipalId, userId).eq(SchoolEntity::getStatus, 1));
		if (schoolEntity!=null){
			roleName = "School_Admin";
		}else {
			GradeClassEntity gradeClassEntity = gradeClassDao.selectOne(new MPJLambdaWrapper<GradeClassEntity>().eq(GradeClassEntity::getPrincipalId, userId).eq(GradeClassEntity::getStatus, 1));
			if (gradeClassEntity!=null){
				classId = gradeClassEntity.getClassId();
				roleName = "Class_Admin";
			}else {
				if (user.getSchoolId()!=null && user.getSchoolId()>0){
					roleName = "Student";
				}
			}
		}
		data.put("roleName",roleName);
		data.put("schoolId",user.getSchoolId());
		data.put("gradeId",user.getGradeId());
		data.put("classId",classId);


		R r = R.ok().put("token", token).put("expire", EXPIRE).put("data",data);

		return r;
	}

	@Override
	public void logout(long userId) {
		//生成一个token
		String token = TokenGenerator.generateValue();

		//修改token
		SysUserTokenEntity tokenEntity = new SysUserTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		this.updateById(tokenEntity);
	}

	public static void main(String[] args) {

		Date now = new Date();
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);
		System.err.println(DateUtils.format(expireTime,DateUtils.DATE_TIME_PATTERN));
	}
}
