package com.exercise.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exercise.common.utils.PageUtils;
import com.exercise.modules.sys.entity.TypeCodeEntity;

import java.util.Map;

/**
 * 数据字典表
 *
 * @author raindrops
 * @email xuzhen0xz@163.com
 * @date 2023-03-09 16:32:24
 */
public interface TypeCodeService extends IService<TypeCodeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

