package com.exercise.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.Query;
import com.exercise.modules.sys.dao.TypeCodeDao;
import com.exercise.modules.sys.entity.TypeCodeEntity;
import com.exercise.modules.sys.service.TypeCodeService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("typeCodeService")
public class TypeCodeServiceImpl extends ServiceImpl<TypeCodeDao, TypeCodeEntity> implements TypeCodeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TypeCodeEntity> page = this.page(
                new Query<TypeCodeEntity>().getPage(params),
                new QueryWrapper<TypeCodeEntity>()
        );

        return new PageUtils(page);
    }

}