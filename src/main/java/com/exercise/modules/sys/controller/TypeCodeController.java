package com.exercise.modules.sys.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.exercise.common.utils.CharUtils;
import com.exercise.modules.exam.entity.KnowEntity;
import com.exercise.modules.exam.entity.KnowListEntity;
import com.github.pagehelper.PageHelper;
import com.exercise.common.utils.PageVO;
import com.exercise.common.utils.R;
import com.exercise.modules.sys.dao.TypeCodeDao;
import com.exercise.modules.sys.entity.TypeCodeEntity;
import com.exercise.modules.sys.entity.form.TypeCodeForm;
import com.exercise.modules.sys.service.TypeCodeService;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * 状态码
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-01-31 15:16:05
 */
@RestController
@RequestMapping("sys/typecode")
public class TypeCodeController {
    @Autowired
    private TypeCodeService typeCodeService;
    private final TypeCodeDao typeCodeDao;

    public TypeCodeController(TypeCodeDao typeCodeDao) {
        this.typeCodeDao = typeCodeDao;
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:typecode:list")
    public R list(TypeCodeForm form) {
        if (form.getParentId() == null) {
            form.setParentId(0);
        }
        PageHelper.startPage(form.getPage(), form.getLimit());
        List<TypeCodeEntity> typeCodeEntities = typeCodeDao.selectList(
                new MPJLambdaWrapper<TypeCodeEntity>()
                        .eq(TypeCodeEntity::getStatus,1)
                        .eq(TypeCodeEntity::getParentId, form.getParentId())
                        .eq(TypeCodeEntity::getType, form.getType())
                        .like(StringUtils.isNotBlank(form.getName()),TypeCodeEntity::getName,form.getName())
                        .eq(form.getGradeId() != null, TypeCodeEntity::getGradeId, form.getGradeId())
                        .orderByAsc(TypeCodeEntity::getWeight));
        return R.ok().put("page", new PageVO<>(typeCodeEntities));
    }

    @RequestMapping("/listAll")
    public R listAll(TypeCodeForm form) {
        if (form.getParentId() == null) {
            form.setParentId(0);
        }
        List<TypeCodeEntity> typeCodeEntities = typeCodeDao.selectList(
                new MPJLambdaWrapper<TypeCodeEntity>()
                        .eq(TypeCodeEntity::getParentId, form.getParentId())
                        .eq(TypeCodeEntity::getType, form.getType())
                        .eq(form.getGradeId() != null, TypeCodeEntity::getGradeId, form.getGradeId())
                        .orderByAsc(TypeCodeEntity::getWeight));
        if (!typeCodeEntities.isEmpty()){
            List<Integer> codeIdList = typeCodeEntities.stream().map(TypeCodeEntity::getId).collect(Collectors.toList());
            List<TypeCodeEntity> subTypeCodeList = typeCodeDao.selectList(new MPJLambdaWrapper<TypeCodeEntity>().in(TypeCodeEntity::getParentId, codeIdList).eq(TypeCodeEntity::getStatus, 1));
            for (TypeCodeEntity typeCodePull : typeCodeEntities) {
                List<TypeCodeEntity> subList = new ArrayList<>();
                typeCodePull.setSubList(subList);
                for (TypeCodeEntity codeEntity : subTypeCodeList) {
                    Integer parentId = codeEntity.getParentId();
                    if (typeCodePull.getId().intValue() == parentId.intValue()) {
                        subList.add(codeEntity);
                    }
                }
            }
        }
        return R.ok().put("data", typeCodeEntities);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:typecode:info")
    public R info(@PathVariable("id") Integer id) {
        TypeCodeEntity typeCode = typeCodeService.getById(id);

        return R.ok().put("typeCode", typeCode);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:typecode:save")
    public R save(@RequestBody TypeCodeEntity typeCode) {
//        String name = typeCode.getName();
//        String parentCode = typeCode.getParentCode();
//        String code = CharUtils.getChineseFirstLetters(name);
//        if (StringUtils.isNotBlank(parentCode)) {
//            code = parentCode + "_" + code;
//        }
//        TypeCodeEntity one = typeCodeService.getOne(new MPJLambdaWrapper<TypeCodeEntity>().eq(TypeCodeEntity::getCode, code));
//        if (one != null) {
//            code = code + "_" + CharUtils.generateRandomString(6);
//        }
//        typeCode.setCode(code);
        typeCodeService.save(typeCode);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:typecode:update")
    public R update(@RequestBody TypeCodeEntity typeCode) {
        typeCodeService.updateById(typeCode);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:typecode:delete")
    public R delete(@RequestBody Integer[] ids) {
        typeCodeService.update(new UpdateWrapper<TypeCodeEntity>().set("status", 0).in("id", Arrays.asList(ids)));

        return R.ok();
    }

}
