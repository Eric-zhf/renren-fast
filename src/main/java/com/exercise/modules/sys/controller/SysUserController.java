/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.exercise.modules.sys.controller;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.exercise.common.utils.CharUtils;
import com.exercise.modules.structure.dao.GradeClassDao;
import com.exercise.modules.structure.dao.SchoolDao;
import com.exercise.modules.structure.entity.GradeClassEntity;
import com.exercise.modules.structure.entity.GradeEntity;
import com.exercise.modules.structure.entity.SchoolEntity;
import com.exercise.modules.sys.entity.SysUserEntity;
import com.exercise.modules.sys.form.PasswordForm;
import com.exercise.modules.sys.service.SysUserService;
import com.exercise.common.annotation.SysLog;
import com.exercise.common.utils.Constant;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.R;
import com.exercise.common.validator.Assert;
import com.exercise.common.validator.ValidatorUtils;
import com.exercise.common.validator.group.AddGroup;
import com.exercise.common.validator.group.UpdateGroup;
import com.exercise.modules.sys.service.SysUserRoleService;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import io.swagger.annotations.ApiOperation;
import lombok.Synchronized;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 系统用户
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;

    private final GradeClassDao gradeClassDao;
    private final SchoolDao schoolDao;

    public SysUserController(GradeClassDao gradeClassDao, SchoolDao schoolDao) {
        this.gradeClassDao = gradeClassDao;
        this.schoolDao = schoolDao;
    }

    /**
     * 所有用户列表
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:user:list")
    public R list(@RequestParam Map<String, Object> params) {
        //只有超级管理员，才能查看所有管理员列表
        if (getUserId() != Constant.SUPER_ADMIN) {
            params.put("createUserId", getUserId());
        }
        PageUtils page = sysUserService.queryPage(params);

        return R.ok().put("page", page);
    }

    @GetMapping("/listUser")
    public R listUser(Integer schoolId,Integer gradeId, Integer classId, String realName,String studentID) {
        int classUserId = 0;
        if (classId!=null){
            GradeClassEntity gradeClassEntity = gradeClassDao.selectById(classId);
            if (gradeClassEntity!=null){
                classUserId = gradeClassEntity.getPrincipalId();
            }
        }
        List<SysUserEntity> list = sysUserService.list(new MPJLambdaWrapper<SysUserEntity>()
                .selectAll(SysUserEntity.class)
                .selectAs(GradeClassEntity::getClassName, SysUserEntity::getClassName)
                .selectAs(GradeEntity::getGradeName, SysUserEntity::getGradeName)
                .selectAs(SchoolEntity::getSchoolName, SysUserEntity::getSchoolName)
                .leftJoin(GradeClassEntity.class, GradeClassEntity::getClassId, SysUserEntity::getClassId)
                .leftJoin(GradeEntity.class, GradeEntity::getGradeId, SysUserEntity::getGradeId)
                .leftJoin(SchoolEntity.class, SchoolEntity::getSchoolId, SysUserEntity::getSchoolId)
                .like(StringUtils.isNotBlank(realName), SysUserEntity::getRealName, realName)
                .like(StringUtils.isNotBlank(studentID), SysUserEntity::getStudentID, studentID)
                .eq(schoolId != null, SysUserEntity::getSchoolId, schoolId)
                .eq(gradeId != null, SysUserEntity::getGradeId, gradeId)
                .eq(classId != null, SysUserEntity::getClassId, classId)
                .ne(classUserId!=0,SysUserEntity::getUserId,classUserId)
                .eq(SysUserEntity::getStatus, 1));
        return R.ok().put("data", list);
    }

    /**
     * 导入
     */
    @PostMapping("/batchImport")
    @Synchronized
    @Transactional(rollbackFor = Exception.class)
    public R batchImport(@RequestParam("classId")Integer classId,
                         @RequestParam("multiFile") MultipartFile multiFile) {
        // 获取文件名
        String fileName = multiFile.getOriginalFilename();
        System.err.println(fileName);
        Assert.isBlank(fileName, "文件获取信息失败");
        // 获取文件后缀
        String prefix = Objects.requireNonNull(fileName).substring(fileName.lastIndexOf("."));
        // 若需要防止生成的临时文件重复,可以在文件名后添加随机码
        File file = null;
        try {
            file = File.createTempFile(fileName, prefix);
            multiFile.transferTo(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (file == null) {
            return R.error("读取失败");
        }

        ExcelReader reader = ExcelUtil.getReader(file);
        List<Map<String, Object>> maps = reader.readAll();
        if (!maps.isEmpty()) {
            GradeClassEntity gradeClassEntity = gradeClassDao.selectById(classId);
            if (gradeClassEntity == null) {
                return R.error("未查询到班级信息");
            }
            Integer schoolId = gradeClassEntity.getSchoolId();
            Integer gradeId = gradeClassEntity.getGradeId();
            SchoolEntity schoolEntity = schoolDao.selectById(schoolId);
            if (schoolEntity == null) {
                return R.error("未查询到学校");
            }
            String schoolName = schoolEntity.getSchoolName();
            String chineseFirstLetters = CharUtils.getChineseFirstLetters(schoolName);
            List<SysUserEntity> list = new ArrayList<>();
            for (Map<String, Object> map : maps) {
                Object nameObj = map.getOrDefault("姓名", "");
                String name = nameObj != null ? nameObj.toString() : "";
                Object studentIdObj = map.getOrDefault("学号", "");
                String studentId = studentIdObj != null ? studentIdObj.toString() : "";
                Object sexObj = map.getOrDefault("性别", "");
                String sex = sexObj != null ? sexObj.toString() : "";
                SysUserEntity user = new SysUserEntity();
                user.setUsername(chineseFirstLetters+"_"+studentId);
                user.setRealName(name);
                user.setPassword(chineseFirstLetters+"_"+studentId);
                user.setSchoolId(schoolId);
                user.setGradeId(gradeId);
                user.setClassId(classId);
                user.setStudentID(studentId);
                user.setGender("男".equals(sex)?0:1);
                user.setStatus(1);
                user.setCreateUserId(getUserId());
                sysUserService.saveUser(user);
            }
            return R.ok();
        } else {
            return R.error("获取excel数据为空，请检查excel文件");
        }

    }

    @ApiOperation("添加用户")
    @RequestMapping("/insertUser")
    public R insertUser(@RequestBody SysUserEntity sysUser) {
        String studentID = sysUser.getStudentID();
        GradeClassEntity gradeClassEntity = gradeClassDao.selectById(sysUser.getClassId());
        if (gradeClassEntity != null) {
            Integer schoolId = gradeClassEntity.getSchoolId();
            SchoolEntity schoolEntity = schoolDao.selectById(schoolId);
            if (schoolEntity != null) {
                String schoolName = schoolEntity.getSchoolName();
                String chineseFirstLetters = CharUtils.getChineseFirstLetters(schoolName);
                sysUser.setUsername(chineseFirstLetters + "_" + studentID);
            }
            sysUser.setSchoolId(schoolId);
            sysUser.setGradeId(gradeClassEntity.getGradeId());
        }
        if (StringUtils.isBlank(sysUser.getUsername())) {
            sysUser.setUsername(studentID);
        }
        if (StringUtils.isBlank(sysUser.getPassword())){
            sysUser.setPassword(sysUser.getUsername());
        }
        sysUserService.saveUser(sysUser);
        return R.ok();
    }

    /**
     * 获取登录的用户信息
     */
    @GetMapping("/info")
    public R info() {
        return R.ok().put("user", getUser());
    }

    /**
     * 修改登录用户密码
     */
    @SysLog("修改密码")
    @PostMapping("/password")
    public R password(@RequestBody PasswordForm form) {
        Assert.isBlank(form.getNewPassword(), "新密码不为能空");

        //sha256加密
        String password = new Sha256Hash(form.getPassword(), getUser().getSalt()).toHex();
        //sha256加密
        String newPassword = new Sha256Hash(form.getNewPassword(), getUser().getSalt()).toHex();

        //更新密码
        boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
        if (!flag) {
            return R.error("原密码不正确");
        }

        return R.ok();
    }

    /**
     * 用户信息
     */
    @GetMapping("/info/{userId}")
    @RequiresPermissions("sys:user:info")
    public R info(@PathVariable("userId") Long userId) {
        SysUserEntity user = sysUserService.getById(userId);

        //获取用户所属的角色列表
        List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
        user.setRoleIdList(roleIdList);

        return R.ok().put("user", user);
    }

    /**
     * 保存用户
     */
    @SysLog("保存用户")
    @PostMapping("/save")
    @RequiresPermissions("sys:user:save")
    public R save(@RequestBody SysUserEntity user) {
        ValidatorUtils.validateEntity(user, AddGroup.class);

        user.setCreateUserId(getUserId());
        sysUserService.saveUser(user);

        return R.ok();
    }

    /**
     * 修改用户
     */
    @SysLog("修改用户")
    @PostMapping("/update")
    @RequiresPermissions("sys:user:update")
    public R update(@RequestBody SysUserEntity user) {
//        ValidatorUtils.validateEntity(user, UpdateGroup.class);

        user.setCreateUserId(getUserId());
        sysUserService.update(user);

        return R.ok();
    }

    /**
     * 删除用户
     */
    @SysLog("删除用户")
    @PostMapping("/delete")
//    @RequiresPermissions("sys:user:delete","sys:myClass")
    public R delete(@RequestBody Long[] userIds) {
        if (ArrayUtils.contains(userIds, 1L)) {
            return R.error("系统管理员不能删除");
        }

        if (ArrayUtils.contains(userIds, getUserId())) {
            return R.error("当前用户不能删除");
        }
        List<SysUserEntity> list = new ArrayList<>();
        for (Long userId : userIds) {
            SysUserEntity user = new SysUserEntity();
            user.setUserId(userId);
            user.setStatus(-1);
            list.add(user);
        }
        sysUserService.updateBatchById(list);

        return R.ok();
    }
}
