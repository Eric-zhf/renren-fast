package com.exercise.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.exercise.modules.sys.entity.TypeCodeEntity;
import com.exercise.modules.sys.entity.TypeCodePullEntity;
import com.exercise.modules.sys.entity.form.TypeCodeForm;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 状态码
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-01-31 15:16:05
 */
@Mapper
public interface TypeCodeDao extends BaseMapper<TypeCodeEntity> {
    /**
     * 根据父级code查询字典
     * @param parentCode 父级code
     * @return list
     * */
    List<TypeCodePullEntity> listTypeCodeByParentCode(String parentCode);

    /**
     * 查询字典列表
     * @param form 查询实体
     * @return list
     * */
    List<TypeCodeEntity> listTypeCode(TypeCodeForm form);



}
