/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.exercise.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.exercise.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 系统用户
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {

    /**
     * 查询用户的所有权限
     * @param userId  用户ID
     */
    List<String> queryAllPerms(Long userId);

    /**
     * 查询用户的所有菜单ID
     */
    List<Long> queryAllMenuId(Long userId);

    /**
     * 根据用户名，查询系统用户
     */
    SysUserEntity queryByUserName(String username);

    int deleteUserBySchoolId(@Param("schoolIds") List<Integer> schoolIds);

    @Update("<script>update sys_user set `status`=0 WHERE user_id IN (SELECT principal_id FROM ep_school WHERE school_id IN )</script>")
    int deleteUserByClassId(@Param("classIds") List<Integer> classIds);

}
