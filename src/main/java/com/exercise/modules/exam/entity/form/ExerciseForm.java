package com.exercise.modules.exam.entity.form;


import com.exercise.common.utils.PageInfo;
import lombok.Data;

/**
 * 题库管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Data
public class ExerciseForm extends PageInfo {
    private Integer examId;
    private Integer chapterId;
    private Integer knowledgePointId;
    private Integer exerciseTypeId;
    private Integer pageNumber;
    private String exerciseIds="";
    private Integer homeworkId;
}
