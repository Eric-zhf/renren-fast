package com.exercise.modules.exam.entity;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: KnowEntity
 * @Description:
 * @Author: raindrops
 * @Date: 2024/2/23 22:47
 */
@Data
public class KnowEntity {
    private Integer knowId;
    private String content;
    private Integer mistakeCount = 0;
    private Integer mistakeSum = 0;
}
