package com.exercise.modules.exam.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 题库管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Data
@TableName("ep_exercise")
public class ExerciseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 题目ID
	 */
	@TableId
	private Integer exerciseId;
	/**
	 * 练习册或试卷ID
	 */
	private Integer examId;
	/**
	 * 章节code_id
	 */
	private Integer chapterId;
	/**
	 * 章节code_id
	 */
	@TableField(exist = false)
	private String chapterName;
	/**
	 * 知识点code_id
	 */
	private Integer knowledgePointId;
	/**
	 * 知识点code_id
	 */
	@TableField(exist = false)
	private String knowledgePointName;
	/**
	 * 题型code_id
	 */
	private Integer exerciseTypeId;
	/**
	 * 题型code_id
	 */
	@TableField(exist = false)
	private String exerciseTypeName;
	/**
	 * 题目文本
	 */
	private String exerciseText;
	/**
	 * 答案文本
	 */
	private String answerText;
	/**
	 * 题目状态，0表示下线，1表示上线
	 */
	private Integer status;
	/**
	 * 排版范围，默认为100%
	 */
	private String layoutRange;
	/**
	 * 页码
	 */
	private Integer pageNumber;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
