package com.exercise.modules.exam.entity.form;


import com.exercise.common.utils.PageInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 作业管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Data
@ApiModel
public class WorkbookForm extends PageInfo {
    @ApiModelProperty("年级ID")
    private Integer gradeId;
    @ApiModelProperty("练习册名称")
    private String bookName;
    private Integer type;
}
