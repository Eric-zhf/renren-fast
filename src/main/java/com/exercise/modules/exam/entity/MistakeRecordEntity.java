package com.exercise.modules.exam.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.Data;

/**
 * 错题管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Data
@TableName("ep_mistake_record")
public class MistakeRecordEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录ID
     */
    @TableId
    private Integer recordId;
    /**
     * 学生ID
     */
    private Integer studentId;
    /**
     * 试卷ID
     */
    private Integer examId;
    /**
     * 错题ID
     */
    private Integer mistakeId;
    /**
     * 错题IDs
     */
    @TableField(exist = false)
    private List<Integer> mistakeIds;
    /**
     * 错误次数
     */
    private Integer mistakeCount;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 创建时间，默认为当前时间
     */
    private Date createTime;
    /**
     * 创建人ID，关联到用户表的主键
     */
    private Integer creatorId;
    /**
     * 修改时间，根据当前时间戳更新
     */
    private Date updateTime;

    public void setMistakeIds(String mistakeIds) {
        if (StringUtils.isNotBlank(mistakeIds)) {
            String[] split = mistakeIds.split(",");
            try {
                this.mistakeIds = Arrays.stream(split).map(Integer::parseInt).collect(Collectors.toList());
            } catch (Exception e) {
                this.mistakeIds = new ArrayList<>();
            }
        } else {
            this.mistakeIds = new ArrayList<>();
        }
    }
}
