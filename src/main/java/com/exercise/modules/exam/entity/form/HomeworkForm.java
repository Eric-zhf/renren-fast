package com.exercise.modules.exam.entity.form;


import com.exercise.common.utils.PageInfo;
import lombok.Data;

/**
 * 作业管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Data
public class HomeworkForm extends PageInfo {
    private String homeworkName;
    private Integer schoolId;
    private Integer gradeId;
    private Integer classId;
    private Integer userId;
}
