package com.exercise.modules.exam.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 作业管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Data
@TableName("ep_homework")
public class HomeworkEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 作业ID
	 */
	@TableId
	private Integer homeworkId;
	private Integer schoolId;
	private Integer gradeId;
	private Integer classId;
	private Integer userId;
	/**
	 * 
	 */
	private String homeworkName;
	/**
	 * 选择的试题IDs
	 */
	private String exerciseIds;
	/**
	 * 创建时间
	 */
	private Date createTime;
	private Integer status;
	/**
	 * 创建人ID
	 */
	private Long creatorId;

	private Integer type;

}
