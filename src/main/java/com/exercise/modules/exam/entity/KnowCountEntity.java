package com.exercise.modules.exam.entity;

import lombok.Data;

/**
 * @ClassName: KnowCountEntity
 * @Description:
 * @Author: raindrops
 * @Date: 2024/2/27 21:22
 */
@Data
public class KnowCountEntity {
    private Integer chapterId;
    private Integer knowledgePointId;
    private Integer countNum;
    private Integer sumNum;
}
