package com.exercise.modules.exam.entity;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: ChapterExerciseEntity
 * @Description:
 * @Author: raindrops
 * @Date: 2024/2/25 18:19
 */
@Data
public class ChapterExerciseEntity {
    private Integer chapterId;
    private String chapterName;
    private List<ExerciseEntity> exerciseList;
}
