package com.exercise.modules.exam.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 练习册管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Data
@TableName("ep_workbook")
public class WorkbookEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 作业ID
	 */
	@TableId
	private Integer bookId;
	private Integer parentId;
	private Integer gradeId;
	/**
	 * 练习册名称
	 */
	private String bookName;
	/**
	 * 练习册封面
	 */
	private String bookCover;
	/**
	 * 创建人ID
	 */
	private Integer status;
	/**
	 * 1习题册，2试卷
	 */
	private Integer type;
	/**
	 * 创建时间
	 */
	private Date createTime;

	private Long creatorId;

	@TableField(exist = false)
	private Integer maxPageNumber;

}
