package com.exercise.modules.exam.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exercise.common.utils.PageUtils;
import com.exercise.modules.exam.dao.ExerciseDao;
import com.exercise.modules.exam.dao.WorkbookDao;
import com.exercise.modules.exam.entity.ExerciseEntity;
import com.exercise.modules.exam.entity.WorkbookEntity;
import com.exercise.modules.exam.entity.form.WorkbookForm;
import com.exercise.modules.exam.service.WorkbookService;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


@Service("workbookService")
public class WorkbookServiceImpl extends ServiceImpl<WorkbookDao, WorkbookEntity> implements WorkbookService {
    private final ExerciseDao exerciseDao;

    public WorkbookServiceImpl(ExerciseDao exerciseDao) {
        this.exerciseDao = exerciseDao;
    }

    @Override
    public PageUtils queryPage(WorkbookForm form) {
        MPJLambdaWrapper<WorkbookEntity> wrapper = new MPJLambdaWrapper<WorkbookEntity>()
                .selectAll(WorkbookEntity.class)
                .eq(form.getGradeId()!=null,WorkbookEntity::getGradeId,form.getGradeId())
                .like(StringUtils.isNotBlank(form.getBookName()),WorkbookEntity::getBookName,form.getBookName())
                .eq(form.getType()!=null,WorkbookEntity::getType,form.getType())
                .gt(WorkbookEntity::getStatus,-1)
                .orderByDesc(WorkbookEntity::getBookId);
        IPage<WorkbookEntity> page = baseMapper.selectJoinPage(
                new Page<>(form.getPage(),form.getLimit()),
                WorkbookEntity.class,
                wrapper
        );
        List<WorkbookEntity> records = page.getRecords();
        if (!records.isEmpty()){
            List<Integer> collect = records.stream().map(WorkbookEntity::getBookId).collect(Collectors.toList());
            List<ExerciseEntity> exerciseEntities = exerciseDao.listMaxPageNumber(collect);
            if (!exerciseEntities.isEmpty()){
                Map<Integer, Integer> pageNumberMap = exerciseEntities.stream().filter(Objects::nonNull).collect(Collectors.toMap(ExerciseEntity::getExamId, ExerciseEntity::getPageNumber));
                for (WorkbookEntity record : records) {
                    Integer bookId = record.getBookId();
                    record.setMaxPageNumber(pageNumberMap.getOrDefault(bookId,0));
                }
            }
        }

        return new PageUtils(page);
    }

}