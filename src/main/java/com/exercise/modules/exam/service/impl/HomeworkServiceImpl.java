package com.exercise.modules.exam.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.Query;

import com.exercise.modules.exam.dao.HomeworkDao;
import com.exercise.modules.exam.entity.HomeworkEntity;
import com.exercise.modules.exam.entity.form.HomeworkForm;
import com.exercise.modules.exam.service.HomeworkService;


@Service("homeworkService")
public class HomeworkServiceImpl extends ServiceImpl<HomeworkDao, HomeworkEntity> implements HomeworkService {

    @Override
    public PageUtils queryPage(HomeworkForm form) {
        MPJLambdaWrapper<HomeworkEntity> wrapper = new MPJLambdaWrapper<HomeworkEntity>()
                .selectAll(HomeworkEntity.class)
                .eq(form.getSchoolId()!=null,HomeworkEntity::getSchoolId,form.getSchoolId())
                .eq(form.getGradeId()!=null,HomeworkEntity::getGradeId,form.getGradeId())
                .eq(form.getClassId()!=null,HomeworkEntity::getClassId,form.getClassId())
                .eq(form.getUserId()!=null,HomeworkEntity::getUserId,form.getUserId())
                .like(StringUtils.isNotBlank(form.getHomeworkName()),HomeworkEntity::getHomeworkName,form.getHomeworkName())
                .eq(HomeworkEntity::getStatus,1)
                .orderByDesc(HomeworkEntity::getHomeworkId);
        if (form.getUserId()==null || form.getUserId()==0){
            wrapper.eq(HomeworkEntity::getUserId,0);
        }
        IPage<HomeworkEntity> page = baseMapper.selectJoinPage(
                new Page<>(form.getPage(),form.getLimit()),
            HomeworkEntity.class,
                wrapper
        );

        return new PageUtils(page);
    }

}