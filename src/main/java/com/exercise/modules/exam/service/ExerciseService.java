package com.exercise.modules.exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.PageVO;
import com.exercise.common.utils.R;
import com.exercise.modules.exam.entity.ChapterExerciseEntity;
import com.exercise.modules.exam.entity.ExerciseEntity;
import com.exercise.modules.exam.entity.form.ExerciseForm;

import java.util.Map;

/**
 * 题库管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
public interface ExerciseService extends IService<ExerciseEntity> {

    PageVO<ChapterExerciseEntity> queryPage(ExerciseForm form);

    R listKnow(Integer homeworkId,Integer classId,Integer studentId);


}

