package com.exercise.modules.exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.PageVO;
import com.exercise.common.utils.R;
import com.exercise.modules.exam.entity.MistakeRecordEntity;
import com.exercise.modules.exam.entity.MistakeRecordVO;
import com.exercise.modules.exam.entity.form.MistakeRecordForm;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 错题管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
public interface MistakeRecordService extends IService<MistakeRecordEntity> {

    PageVO<MistakeRecordVO> queryPage(MistakeRecordForm form);

    R saveRecord(MistakeRecordEntity record);
}

