package com.exercise.modules.exam.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.exercise.common.utils.PageVO;
import com.exercise.common.utils.R;
import com.exercise.modules.exam.dao.HomeworkDao;
import com.exercise.modules.exam.dao.MistakeRecordDao;
import com.exercise.modules.exam.dao.WorkbookDao;
import com.exercise.modules.exam.entity.*;
import com.exercise.modules.structure.dao.GradeClassDao;
import com.exercise.modules.structure.entity.GradeClassEntity;
import com.exercise.modules.sys.dao.TypeCodeDao;
import com.exercise.modules.sys.entity.TypeCodeEntity;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;
import com.github.yulichang.wrapper.MPJLambdaWrapper;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.exercise.modules.exam.dao.ExerciseDao;
import com.exercise.modules.exam.entity.form.ExerciseForm;
import com.exercise.modules.exam.service.ExerciseService;


@Service("exerciseService")
public class ExerciseServiceImpl extends ServiceImpl<ExerciseDao, ExerciseEntity> implements ExerciseService {

    private final TypeCodeDao typeCodeDao;
    private final HomeworkDao homeworkDao;
    private final MistakeRecordDao mistakeRecordDao;
    private final GradeClassDao gradeClassDao;
    private final WorkbookDao workbookDao;

    public ExerciseServiceImpl(TypeCodeDao typeCodeDao, HomeworkDao homeworkDao, MistakeRecordDao mistakeRecordDao, GradeClassDao gradeClassDao, WorkbookDao workbookDao) {
        this.typeCodeDao = typeCodeDao;
        this.homeworkDao = homeworkDao;
        this.mistakeRecordDao = mistakeRecordDao;
        this.gradeClassDao = gradeClassDao;
        this.workbookDao = workbookDao;
    }

    @Override
    public PageVO<ChapterExerciseEntity> queryPage(ExerciseForm form) {
        boolean isPageNumber=true;
        Integer examId = form.getExamId();
        if (examId!=null){
            WorkbookEntity workbook = workbookDao.selectById(examId);
            Integer type = workbook.getType();
            isPageNumber = (type==1);
        }
        Integer homeworkId = form.getHomeworkId();
        HomeworkEntity homework = new HomeworkEntity();
        if (homeworkId!=null){
            homework = homeworkDao.selectById(homeworkId);
            if(homework!=null){
                Integer type = homework.getType();
                isPageNumber = (type==1);
            }
        }
//        boolean isPageNumber = form.getPageNumber() != null;
        boolean notBlank = StringUtils.isNotBlank(form.getExerciseIds());
        MPJLambdaWrapper<ExerciseEntity> countWrapper = new MPJLambdaWrapper<ExerciseEntity>()
                .eq(examId != null, ExerciseEntity::getExamId, examId)
                .eq(form.getChapterId() != null, ExerciseEntity::getChapterId, form.getChapterId())
                .eq(form.getKnowledgePointId() != null, ExerciseEntity::getKnowledgePointId, form.getKnowledgePointId())
                .eq(form.getExerciseTypeId() != null, ExerciseEntity::getExerciseTypeId, form.getExerciseTypeId())
                .gt(ExerciseEntity::getStatus,-1)
                .in(notBlank, ExerciseEntity::getExerciseId, Arrays.asList(form.getExerciseIds().split(",")));
        Long totalCount = this.baseMapper.selectCount(countWrapper);
        PageVO<ChapterExerciseEntity> pageVO = new PageVO<>();
        if (totalCount==null || totalCount==0){
            pageVO.init(0,1,new ArrayList<>());
            return pageVO;
        }
        Integer examMaxPage = 1;
        if (isPageNumber && examId!=null){
            examMaxPage = this.baseMapper.getMaxPageNumber(examId);
        }
        MPJLambdaWrapper<ExerciseEntity> wrapper = new MPJLambdaWrapper<ExerciseEntity>()
                .selectAll(ExerciseEntity.class)
                .eq(examId != null, ExerciseEntity::getExamId, examId)
                .eq(form.getChapterId() != null, ExerciseEntity::getChapterId, form.getChapterId())
                .eq(form.getKnowledgePointId() != null, ExerciseEntity::getKnowledgePointId, form.getKnowledgePointId())
                .eq(form.getPageNumber()!=null, ExerciseEntity::getPageNumber, form.getPageNumber())
                .eq(form.getExerciseTypeId() != null, ExerciseEntity::getExerciseTypeId, form.getExerciseTypeId())
                .gt(ExerciseEntity::getStatus,-1)
                .in(notBlank, ExerciseEntity::getExerciseId, Arrays.asList(form.getExerciseIds().split(",")));
        List<Integer> sortList = new ArrayList<>();
        if (notBlank){
            sortList = Arrays.stream(form.getExerciseIds().split(",")).map(Integer::parseInt).collect(Collectors.toList());
        }
        if (homeworkId != null) {
            wrapper.leftJoin("ep_homework ehw ON FIND_IN_SET(t.exercise_id,ehw.exercise_ids)").eq("ehw.homework_id", homeworkId);
//            HomeworkEntity homework = homeworkDao.selectById(homeworkId);
            if (homework!=null){
                sortList = Arrays.stream(homework.getExerciseIds().split(",")).map(Integer::parseInt).collect(Collectors.toList());
            }
        }
        List<ExerciseEntity> records = this.baseMapper.selectList(wrapper);
        setTypeCode(records);
        Map<Integer, List<ExerciseEntity>> exerciseMap;
//        if (form.getPageNumber()!=null && form.getPageNumber()>0){
        if (isPageNumber){
            exerciseMap = records.stream().collect(Collectors.groupingBy(ExerciseEntity::getChapterId));
        }else {
            exerciseMap = records.stream().collect(Collectors.groupingBy(ExerciseEntity::getExerciseTypeId));
        }
        List<ChapterExerciseEntity> list = new ArrayList<>();
        List<Integer> finalSortList = sortList;
        boolean finalIsPageNumber = isPageNumber;
        exerciseMap.forEach((chatId, chats)->{
            ChapterExerciseEntity exercise = new ChapterExerciseEntity();
            String chapterName;
//            if (form.getPageNumber()!=null && form.getPageNumber()>0){
            if (finalIsPageNumber){
                chapterName = chats.get(0).getChapterName();
            }else {
                chapterName = chats.get(0).getExerciseTypeName();
            }
            exercise.setChapterId(chatId);
            exercise.setChapterName(chapterName);
            if (!finalSortList.isEmpty()){
                chats.sort(Comparator.comparingInt(obj -> {
                    int index = finalSortList.indexOf(obj.getExerciseId());
                    return index == -1 ? Integer.MAX_VALUE : index;
                }));
            }
            exercise.setExerciseList(chats);
            list.add(exercise);
        });
        Integer current = form.getPage();
        if (isPageNumber){
            if (form.getPageNumber()!=null  && form.getPageNumber()>0){
                current = form.getPageNumber();
            }
        }
        if (current==null){
            current=1;
        }
        pageVO.init(totalCount,current,examMaxPage,list);
        return pageVO;
    }

    @Override
    public R listKnow(Integer homeworkId,Integer classId,Integer studentId) {
        List<KnowListEntity> list = new ArrayList<>();
        if (homeworkId != null) {

        } else {
            int gradeId = 0;
            if (classId!=null){
                GradeClassEntity gradeClassEntity = gradeClassDao.selectById(classId);
                if (gradeClassEntity!=null){
                    gradeId = gradeClassEntity.getGradeId();
                }
            }
            List<KnowCountEntity> knowCountEntities = mistakeRecordDao.countKnowByChapter(classId, studentId);
            Map<Integer, Integer> chapterCountMap = Maps.newHashMap();
            Map<Integer, Integer> chapterSumMap = Maps.newHashMap();
            Map<Integer, Integer> knowCountMap = Maps.newHashMap();
            Map<Integer, Integer> knowSumMap = Maps.newHashMap();
            if (!knowCountEntities.isEmpty()){
                chapterCountMap = knowCountEntities.stream().collect(Collectors.groupingBy(KnowCountEntity::getChapterId, Collectors.summingInt(KnowCountEntity::getCountNum)));
                chapterSumMap = knowCountEntities.stream().collect(Collectors.groupingBy(KnowCountEntity::getChapterId, Collectors.summingInt(KnowCountEntity::getSumNum)));
                knowCountMap = knowCountEntities.stream().collect(Collectors.groupingBy(KnowCountEntity::getKnowledgePointId, Collectors.summingInt(KnowCountEntity::getCountNum)));
                knowSumMap = knowCountEntities.stream().collect(Collectors.groupingBy(KnowCountEntity::getKnowledgePointId, Collectors.summingInt(KnowCountEntity::getSumNum)));
            }
            MPJLambdaWrapper<TypeCodeEntity> wrapper = new MPJLambdaWrapper<TypeCodeEntity>().eq(TypeCodeEntity::getType, 1).eq(TypeCodeEntity::getStatus, 1).eq(TypeCodeEntity::getParentId, 0);
            if (gradeId!=0){
                wrapper.eq(TypeCodeEntity::getGradeId,gradeId);
            }
            wrapper.orderByAsc(TypeCodeEntity::getWeight);
            List<TypeCodeEntity> chapter = typeCodeDao.selectList(wrapper);
            if (!chapter.isEmpty()) {
                List<Integer> codeIdList = chapter.stream().map(TypeCodeEntity::getId).collect(Collectors.toList());
                List<TypeCodeEntity> subTypeCodeList = typeCodeDao.selectList(new MPJLambdaWrapper<TypeCodeEntity>().in(TypeCodeEntity::getParentId, codeIdList).eq(TypeCodeEntity::getStatus, 1).orderByAsc(TypeCodeEntity::getWeight));
                for (TypeCodeEntity typeCodePull : chapter) {
                    KnowListEntity knowEntity = new KnowListEntity();
                    list.add(knowEntity);
                    Integer id = typeCodePull.getId();
                    knowEntity.setKnowId(id);
                    knowEntity.setContent(typeCodePull.getName());
                    knowEntity.setMistakeCount(chapterCountMap.getOrDefault(id,0));
                    knowEntity.setMistakeSum(chapterSumMap.getOrDefault(id,0));
                    List<KnowEntity> subList = new ArrayList<>();
                    knowEntity.setSubKnowList(subList);
                    for (TypeCodeEntity codeEntity : subTypeCodeList) {
                        Integer parentId = codeEntity.getParentId();
                        if (id.intValue() == parentId.intValue()) {
                            KnowEntity subKnow = new KnowEntity();
                            subList.add(subKnow);
                            subKnow.setKnowId(codeEntity.getId());
                            subKnow.setContent(codeEntity.getName());
                            subKnow.setMistakeCount(knowCountMap.getOrDefault(codeEntity.getId(),0));
                            subKnow.setMistakeSum(knowSumMap.getOrDefault(codeEntity.getId(),0));
                        }
                    }
                }
            }
        }
        return R.ok().put("data", list);
    }

    public void setTypeCode(List<ExerciseEntity> exerciseList) {
        if (!exerciseList.isEmpty()) {
            List<Integer> chapterIdList = exerciseList.stream().map(ExerciseEntity::getChapterId).collect(Collectors.toList());
            List<Integer> knowIdList = exerciseList.stream().map(ExerciseEntity::getKnowledgePointId).collect(Collectors.toList());
            List<Integer> typeIdList = exerciseList.stream().map(ExerciseEntity::getExerciseTypeId).collect(Collectors.toList());
            Map<Integer, String> chapterMap = getTypeCodeMap(chapterIdList);
            Map<Integer, String> knowMap = getTypeCodeMap(knowIdList);
            Map<Integer, String> typeMap = getTypeCodeMap(typeIdList);
            for (ExerciseEntity record : exerciseList) {
                Integer chapterId = record.getChapterId();
                Integer knowledgePointId = record.getKnowledgePointId();
                Integer exerciseTypeId = record.getExerciseTypeId();
                record.setChapterName(chapterMap.getOrDefault(chapterId, ""));
                record.setKnowledgePointName(knowMap.getOrDefault(knowledgePointId, ""));
                record.setExerciseTypeName(typeMap.getOrDefault(exerciseTypeId, ""));
            }
        }
    }

    private Map<Integer, String> getTypeCodeMap(List<Integer> typeIdList) {
        List<TypeCodeEntity> typeCodeList = typeCodeDao.selectList(new MPJLambdaWrapper<TypeCodeEntity>().in(TypeCodeEntity::getId, typeIdList).orderByAsc(TypeCodeEntity::getWeight));
        Map<Integer, String> typeCodeMap = Maps.newHashMap();
        if (!typeCodeList.isEmpty()) {
            typeCodeMap = typeCodeList.stream().collect(Collectors.toMap(TypeCodeEntity::getId, TypeCodeEntity::getName));
        }
        return typeCodeMap;
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        // 假设 list 包含 MyObject 对象
        list.add(1);
        list.add(7);
        list.add(2);
        list.add(4);
        list.add(6);
        List<Integer> order = Arrays.asList(2, 1, 6, 4);

        // 使用 Comparator 自定义排序
        list.sort(Comparator.comparingInt(obj -> {
            int index = order.indexOf(obj);
            return index == -1 ? Integer.MAX_VALUE : index;
        }));
        System.err.println(JSONObject.toJSONString(list));
    }

}