package com.exercise.modules.exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exercise.common.utils.PageUtils;
import com.exercise.modules.exam.entity.HomeworkEntity;
import com.exercise.modules.exam.entity.WorkbookEntity;
import com.exercise.modules.exam.entity.form.HomeworkForm;
import com.exercise.modules.exam.entity.form.WorkbookForm;

/**
 * 作业管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
public interface WorkbookService extends IService<WorkbookEntity> {

    PageUtils queryPage(WorkbookForm form);
}

