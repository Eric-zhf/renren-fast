package com.exercise.modules.exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exercise.common.utils.PageUtils;
import com.exercise.modules.exam.entity.HomeworkEntity;
import com.exercise.modules.exam.entity.form.HomeworkForm;

import java.util.Map;

/**
 * 作业管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
public interface HomeworkService extends IService<HomeworkEntity> {

    PageUtils queryPage(HomeworkForm form);
}

