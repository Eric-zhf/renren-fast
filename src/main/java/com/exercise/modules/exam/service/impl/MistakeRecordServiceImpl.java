package com.exercise.modules.exam.service.impl;

import com.exercise.common.utils.PageVO;
import com.exercise.common.utils.R;
import com.exercise.modules.exam.entity.ExerciseEntity;
import com.exercise.modules.exam.entity.MistakeRecordVO;
import com.exercise.modules.sys.dao.SysUserDao;
import com.exercise.modules.sys.dao.TypeCodeDao;
import com.exercise.modules.sys.entity.TypeCodeEntity;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.exercise.modules.exam.dao.MistakeRecordDao;
import com.exercise.modules.exam.entity.MistakeRecordEntity;
import com.exercise.modules.exam.entity.form.MistakeRecordForm;
import com.exercise.modules.exam.service.MistakeRecordService;


@Service("mistakeRecordService")
public class MistakeRecordServiceImpl extends ServiceImpl<MistakeRecordDao, MistakeRecordEntity> implements MistakeRecordService {

    private final SysUserDao sysUserDao;
    private final TypeCodeDao typeCodeDao;

    public MistakeRecordServiceImpl(SysUserDao sysUserDao, TypeCodeDao typeCodeDao) {
        this.sysUserDao = sysUserDao;
        this.typeCodeDao = typeCodeDao;
    }

    @Override
    public PageVO<MistakeRecordVO> queryPage(MistakeRecordForm form) {
        Integer countRecord = this.baseMapper.countRecord(form);
        PageVO<MistakeRecordVO> pageVO = new PageVO<>();
        if (countRecord<=0){
            pageVO.init(0,form.getPage(),new ArrayList<>());
            return pageVO;
        }
        List<MistakeRecordVO> recordVOList = this.baseMapper.listMistakeExercise(form);
//        if (!recordVOList.isEmpty()){
//            String studentIds = recordVOList.stream().map(MistakeRecordVO::getStudentIds).collect(Collectors.joining(","));
//            List<Integer> studentIdList = Arrays.stream(studentIds.split(","))
//                    .map(Integer::parseInt)
//                    .collect(Collectors.toList());
//        }
        setTypeCode(recordVOList);
        pageVO.init(countRecord, form.getPage(), recordVOList);
        return pageVO;
    }

    @Override
    public R saveRecord(MistakeRecordEntity record) {
        List<Integer> mistakeIds = record.getMistakeIds();
        if (mistakeIds.isEmpty()){
            return R.error("请选择错题");
        }
        this.baseMapper.saveRecord(record);
        return R.ok();
    }

    public void setTypeCode(List<MistakeRecordVO> exerciseList) {
        if (!exerciseList.isEmpty()) {
            List<Integer> chapterIdList = exerciseList.stream().map(ExerciseEntity::getChapterId).collect(Collectors.toList());
            List<Integer> knowIdList = exerciseList.stream().map(ExerciseEntity::getKnowledgePointId).collect(Collectors.toList());
            List<Integer> typeIdList = exerciseList.stream().map(ExerciseEntity::getExerciseTypeId).collect(Collectors.toList());
            Map<Integer, String> chapterMap = getTypeCodeMap(chapterIdList);
            Map<Integer, String> knowMap = getTypeCodeMap(knowIdList);
            Map<Integer, String> typeMap = getTypeCodeMap(typeIdList);
            for (ExerciseEntity record : exerciseList) {
                Integer chapterId = record.getChapterId();
                Integer knowledgePointId = record.getKnowledgePointId();
                Integer exerciseTypeId = record.getExerciseTypeId();
                record.setChapterName(chapterMap.getOrDefault(chapterId, ""));
                record.setKnowledgePointName(knowMap.getOrDefault(knowledgePointId, ""));
                record.setExerciseTypeName(typeMap.getOrDefault(exerciseTypeId, ""));
            }
        }
    }

    private Map<Integer, String> getTypeCodeMap(List<Integer> typeIdList) {
        List<TypeCodeEntity> typeCodeList = typeCodeDao.selectList(new MPJLambdaWrapper<TypeCodeEntity>().in(TypeCodeEntity::getId, typeIdList).orderByAsc(TypeCodeEntity::getWeight));
        Map<Integer, String> typeCodeMap = Maps.newHashMap();
        if (!typeCodeList.isEmpty()) {
            typeCodeMap = typeCodeList.stream().collect(Collectors.toMap(TypeCodeEntity::getId, TypeCodeEntity::getName));
        }
        return typeCodeMap;
    }

}