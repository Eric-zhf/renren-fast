package com.exercise.modules.exam.dao;

import com.exercise.modules.exam.entity.KnowCountEntity;
import com.exercise.modules.exam.entity.KnowEntity;
import com.exercise.modules.exam.entity.MistakeRecordEntity;
import com.exercise.modules.exam.entity.MistakeRecordVO;
import com.exercise.modules.exam.entity.form.MistakeRecordForm;
import org.apache.ibatis.annotations.Mapper;
import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 错题管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Mapper
public interface MistakeRecordDao extends MPJBaseMapper<MistakeRecordEntity> {

    Integer saveRecord(MistakeRecordEntity record);


    Integer countRecord(MistakeRecordForm form);

    List<MistakeRecordVO> listMistakeExercise(MistakeRecordForm form);

    List<KnowCountEntity> countKnowByChapter(@Param("classId") Integer classId,
                                             @Param("studentId") Integer studentId);
}
