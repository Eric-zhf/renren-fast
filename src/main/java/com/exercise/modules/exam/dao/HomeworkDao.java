package com.exercise.modules.exam.dao;

import com.exercise.modules.exam.entity.HomeworkEntity;
import org.apache.ibatis.annotations.Mapper;
import com.github.yulichang.base.MPJBaseMapper;

/**
 * 作业管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Mapper
public interface HomeworkDao extends MPJBaseMapper<HomeworkEntity> {
	
}
