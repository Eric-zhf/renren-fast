package com.exercise.modules.exam.dao;

import com.exercise.modules.exam.entity.ExerciseEntity;
import org.apache.ibatis.annotations.Mapper;
import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 题库管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Mapper
public interface ExerciseDao extends MPJBaseMapper<ExerciseEntity> {

    @Select("SELECT MAX(page_number) FROM ep_exercise WHERE exam_id=#{examId} AND status=1")
    Integer getMaxPageNumber(Integer examId);

    @Select("<script>" +
            "SELECT exam_id,MAX(page_number) pageNumber FROM ep_exercise WHERE exam_id IN " +
            "<foreach item=\"item\" collection=\"listId\" open=\"(\" separator=\",\" close=\")\">" +
            " #{item} " +
            "</foreach>" +
            " AND status=1" +
            "</script>")
    List<ExerciseEntity> listMaxPageNumber(@Param("listId") List<Integer> listId);
}
