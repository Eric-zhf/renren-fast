package com.exercise.modules.exam.dao;

import com.exercise.modules.exam.entity.WorkbookEntity;
import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 作业管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@Mapper
public interface WorkbookDao extends MPJBaseMapper<WorkbookEntity> {
	
}
