package com.exercise.modules.exam.controller;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.R;
import com.exercise.modules.exam.dao.ExerciseDao;
import com.exercise.modules.exam.entity.ExerciseEntity;
import com.exercise.modules.exam.entity.HomeworkEntity;
import com.exercise.modules.exam.entity.WorkbookEntity;
import com.exercise.modules.exam.entity.form.HomeworkForm;
import com.exercise.modules.exam.entity.form.WorkbookForm;
import com.exercise.modules.exam.service.HomeworkService;
import com.exercise.modules.exam.service.WorkbookService;
import com.exercise.modules.sys.controller.AbstractController;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;


/**
 * 练习册管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@RestController
@RequestMapping("exam/workbook")
public class WorkbookController extends AbstractController {
    @Autowired
    private WorkbookService workbookService;
    @Autowired
    private ExerciseDao exerciseDao;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(WorkbookForm form){
        PageUtils page = workbookService.queryPage(form);
        return R.ok().put("page", page);
    }

    @GetMapping("/listAll")
    public R listAll(WorkbookForm form){
        List<WorkbookEntity> list = workbookService.list(new MPJLambdaWrapper<WorkbookEntity>()
                .eq(WorkbookEntity::getStatus,1)
                .eq(form.getGradeId()!=null,WorkbookEntity::getGradeId,form.getGradeId())
                .like(StringUtils.isNotBlank(form.getBookName()),WorkbookEntity::getBookName,form.getBookName())
                .eq(form.getType()!=null,WorkbookEntity::getType,form.getType())
                .orderByDesc(WorkbookEntity::getBookId));
        if (!list.isEmpty()){
            List<Integer> collect = list.stream().map(WorkbookEntity::getBookId).collect(Collectors.toList());
            List<ExerciseEntity> exerciseEntities = exerciseDao.listMaxPageNumber(collect);
            if (exerciseEntities!=null &&!exerciseEntities.isEmpty()){
                Map<Integer, Integer> pageNumberMap = exerciseEntities.stream().filter(Objects::nonNull).collect(Collectors.toMap(ExerciseEntity::getExamId, ExerciseEntity::getPageNumber));
                for (WorkbookEntity record : list) {
                    Integer bookId = record.getBookId();
                    record.setMaxPageNumber(pageNumberMap.getOrDefault(bookId,0));
                }
            }
        }
        return R.ok().put("data", list);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{workbookId}")
    public R info(@PathVariable("workbookId") Integer workbookId){
        WorkbookEntity workbook = workbookService.getById(workbookId);

        return R.ok().put("workbook", workbook);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody WorkbookEntity workbook){
        workbook.setCreatorId(getUserId());
        workbookService.save(workbook);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody WorkbookEntity workbook){
        workbookService.updateById(workbook);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] bookIds){
        List<WorkbookEntity> workbookEntities = workbookService.listByIds(Arrays.asList(bookIds));
        if (!workbookEntities.isEmpty()){
            Long userId = getUserId();
            for (WorkbookEntity workbookEntity : workbookEntities) {
                Long creatorId = workbookEntity.getCreatorId();
                if (creatorId.longValue()!=userId.longValue()){
                    return R.error("只能删除自己创建的习题册试卷");
                }
            }
        }else {
            return R.error("不能删除不存在的数据");
        }
        List<WorkbookEntity> list = new ArrayList<>();
        for (Integer bookId : bookIds) {
            WorkbookEntity workbook = new WorkbookEntity();
            workbook.setBookId(bookId);
            workbook.setStatus(-1);
            list.add(workbook);
        }
        workbookService.updateBatchById(list);

        return R.ok();
    }

}
