package com.exercise.modules.exam.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.exercise.common.utils.PageVO;
import com.exercise.modules.exam.entity.ChapterExerciseEntity;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exercise.modules.exam.entity.ExerciseEntity;
import com.exercise.modules.exam.entity.form.ExerciseForm;
import com.exercise.modules.exam.service.ExerciseService;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.R;



/**
 * 题库管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@RestController
@RequestMapping("exam/exercise")
public class ExerciseController {
    @Autowired
    private ExerciseService exerciseService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("exam:exercise:list")
    public R list(ExerciseForm form){
        PageVO<ChapterExerciseEntity> page = exerciseService.queryPage(form);

        return R.ok().put("page", page);
    }

    @RequestMapping("/listKnow")
    public R listKnow(Integer homeworkId,Integer classId,Integer studentId){
        return exerciseService.listKnow(homeworkId, classId, studentId);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{exerciseId}")
    @RequiresPermissions("exam:exercise:info")
    public R info(@PathVariable("exerciseId") Integer exerciseId){
		ExerciseEntity exercise = exerciseService.getById(exerciseId);

        return R.ok().put("exercise", exercise);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("exam:exercise:save")
    public R save(@RequestBody ExerciseEntity exercise){
		exerciseService.save(exercise);

        return R.ok().put("data",exercise.getExerciseId());
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("exam:exercise:update")
    public R update(@RequestBody ExerciseEntity exercise){
		exerciseService.updateById(exercise);

        return R.ok().put("data",exercise.getExerciseId());
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("exam:exercise:delete")
    public R delete(@RequestBody Integer[] exerciseIds){
        List<ExerciseEntity> list = new ArrayList<>();
        for (Integer exerciseId : exerciseIds) {
            ExerciseEntity exercise = new ExerciseEntity();
            exercise.setExerciseId(exerciseId);
            exercise.setStatus(-1);
            list.add(exercise);
        }
		exerciseService.updateBatchById(list);

        return R.ok();
    }

}
