package com.exercise.modules.exam.controller;

import java.util.Arrays;
import java.util.Map;

import com.exercise.common.utils.PageVO;
import com.exercise.modules.exam.entity.MistakeRecordVO;
import com.exercise.modules.sys.controller.AbstractController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exercise.modules.exam.entity.MistakeRecordEntity;
import com.exercise.modules.exam.entity.form.MistakeRecordForm;
import com.exercise.modules.exam.service.MistakeRecordService;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.R;



/**
 * 错题管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@RestController
@RequestMapping("exam/mistakerecord")
public class MistakeRecordController extends AbstractController {
    @Autowired
    private MistakeRecordService mistakeRecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("exam:mistakerecord:list")
    public R list(MistakeRecordForm form){
        PageVO<MistakeRecordVO> page = mistakeRecordService.queryPage(form);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{recordId}")
    @RequiresPermissions("exam:mistakerecord:info")
    public R info(@PathVariable("recordId") Integer recordId){
		MistakeRecordEntity mistakeRecord = mistakeRecordService.getById(recordId);

        return R.ok().put("mistakeRecord", mistakeRecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("exam:mistakerecord:save")
    public R save(@RequestBody MistakeRecordEntity mistakeRecord){
        Long userId = getUserId();
        mistakeRecord.setCreatorId(Integer.parseInt(userId.toString()));
		mistakeRecordService.saveRecord(mistakeRecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("exam:mistakerecord:update")
    public R update(@RequestBody MistakeRecordEntity mistakeRecord){
        mistakeRecordService.updateById(mistakeRecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("exam:mistakerecord:delete")
    public R delete(@RequestBody Integer[] recordIds){
		mistakeRecordService.removeByIds(Arrays.asList(recordIds));

        return R.ok();
    }

}
