package com.exercise.modules.exam.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

import com.exercise.common.utils.PageVO;
import com.exercise.modules.exam.entity.ChapterExerciseEntity;
import com.exercise.modules.exam.entity.WorkbookEntity;
import com.exercise.modules.exam.entity.form.ExerciseForm;
import com.exercise.modules.sys.controller.AbstractController;
import freemarker.core.ParseException;
import freemarker.template.*;

import com.exercise.modules.exam.service.ExerciseService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.exercise.modules.exam.entity.HomeworkEntity;
import com.exercise.modules.exam.entity.form.HomeworkForm;
import com.exercise.modules.exam.service.HomeworkService;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.R;



/**
 * 作业管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:19:01
 */
@RestController
@RequestMapping("exam/homework")
public class HomeworkController extends AbstractController {
    @Autowired
    private HomeworkService homeworkService;
    @Autowired
    private ExerciseService exerciseService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("exam:homework:list")
    public R list(HomeworkForm form){
        PageUtils page = homeworkService.queryPage(form);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{homeworkId}")
    @RequiresPermissions("exam:homework:info")
    public R info(@PathVariable("homeworkId") Integer homeworkId){
		HomeworkEntity homework = homeworkService.getById(homeworkId);

        return R.ok().put("homework", homework);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("exam:homework:save")
    public R save(@RequestBody HomeworkEntity homework){
        homework.setCreatorId(getUserId());
		homeworkService.save(homework);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("exam:homework:update")
    public R update(@RequestBody HomeworkEntity homework){
		homeworkService.updateById(homework);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("exam:homework:delete")
    public R delete(@RequestBody Integer[] homeworkIds){
        List<HomeworkEntity> homeworkEntities = homeworkService.listByIds(Arrays.asList(homeworkIds));
        if (!homeworkEntities.isEmpty()){
            Long userId = getUserId();
            for (HomeworkEntity homeworkEntity : homeworkEntities) {
                Long creatorId = homeworkEntity.getCreatorId();
                if (creatorId.longValue()!=userId.longValue()){
                    return R.error("只能删除自己创建的作业");
                }
            }
        }else {
            return R.error("不能删除不存在的数据");
        }
        List<HomeworkEntity> list = new ArrayList<>();
        for (Integer homeworkId : homeworkIds) {
            HomeworkEntity homework = new HomeworkEntity();
            homework.setHomeworkId(homeworkId);
            homework.setStatus(-1);
            list.add(homework);
        }
        homeworkService.updateBatchById(list);
        return R.ok();
    }

}
