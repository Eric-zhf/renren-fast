package com.exercise.modules.structure.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 班级管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
@Data
@TableName("ep_grade_class")
public class GradeClassEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 班级ID
	 */
	@TableId
	private Integer classId;
	/**
	 * 班级名称
	 */
	private String className;
	/**
	 * 所在年级ID
	 */
	private Integer gradeId;
	@TableField(exist = false)
	private String gradeName;
	/**
	 * 学校ID
	 */
	private Integer schoolId;
	@TableField(exist = false)
	private String schoolName;
	/**
	 * 班级负责人ID
	 */
	private Integer principalId;
	/**
	 * 班级负责人ID
	 */
	@TableField(exist = false)
	private String principalName;
	/**
	 * 班级状态，0表示非激活，1表示激活
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
