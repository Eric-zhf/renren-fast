package com.exercise.modules.structure.entity.form;


import com.exercise.common.utils.PageInfo;
import lombok.Data;

/**
 * 年级管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
@Data
public class GradeForm extends PageInfo {

}
