package com.exercise.modules.structure.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 年级管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
@Data
@TableName("ep_grade")
public class GradeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 年级ID
	 */
	@TableId
	private Integer gradeId;

	private String gradeName;
	/**
	 * 年级状态，0表示非激活，1表示激活
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
