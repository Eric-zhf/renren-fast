package com.exercise.modules.structure.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 学校管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
@Data
@TableName("ep_school")
public class SchoolEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 学校ID
	 */
	@TableId
	private Integer schoolId;
	/**
	 * 学校名
	 */
	private String schoolName;
	/**
	 * 学校负责人ID
	 */
	private Integer principalId;
	@TableField(exist = false)
	private String principalName;
	/**
	 * 学校状态，0表示未在使用，1表示在使用
	 */
	private Integer status;
	/**
	 * 创建时间，默认为当前时间
	 */
	private Date createTime;

}
