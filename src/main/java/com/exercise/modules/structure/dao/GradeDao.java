package com.exercise.modules.structure.dao;

import com.exercise.modules.structure.entity.GradeEntity;
import org.apache.ibatis.annotations.Mapper;
import com.github.yulichang.base.MPJBaseMapper;

/**
 * 年级管理
 * 
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
@Mapper
public interface GradeDao extends MPJBaseMapper<GradeEntity> {
	
}
