package com.exercise.modules.structure.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exercise.common.utils.PageUtils;
import com.exercise.modules.structure.entity.GradeEntity;
import com.exercise.modules.structure.entity.form.GradeForm;

import java.util.Map;

/**
 * 年级管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
public interface GradeService extends IService<GradeEntity> {

    PageUtils queryPage(GradeForm form);
}

