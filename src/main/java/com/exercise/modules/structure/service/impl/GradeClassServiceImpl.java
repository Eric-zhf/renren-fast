package com.exercise.modules.structure.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exercise.modules.structure.entity.GradeEntity;
import com.exercise.modules.structure.entity.SchoolEntity;
import com.exercise.modules.sys.entity.SysUserEntity;
import org.springframework.stereotype.Service;
import com.github.yulichang.wrapper.MPJLambdaWrapper;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.Query;

import com.exercise.modules.structure.dao.GradeClassDao;
import com.exercise.modules.structure.entity.GradeClassEntity;
import com.exercise.modules.structure.entity.form.GradeClassForm;
import com.exercise.modules.structure.service.GradeClassService;


@Service("gradeClassService")
public class GradeClassServiceImpl extends ServiceImpl<GradeClassDao, GradeClassEntity> implements GradeClassService {

    @Override
    public PageUtils queryPage(GradeClassForm form) {
        MPJLambdaWrapper<GradeClassEntity> wrapper = new MPJLambdaWrapper<GradeClassEntity>()
                .selectAll(GradeClassEntity.class)
                .selectAs(GradeEntity::getGradeName, GradeClassEntity::getGradeName)
                .selectAs(SchoolEntity::getSchoolName, GradeClassEntity::getSchoolName)
                .selectAs(SysUserEntity::getUsername, GradeClassEntity::getPrincipalName)
                .leftJoin(GradeEntity.class, GradeEntity::getGradeId, GradeClassEntity::getGradeId)
                .leftJoin(SchoolEntity.class, SchoolEntity::getSchoolId, GradeClassEntity::getSchoolId)
                .leftJoin(SysUserEntity.class,SysUserEntity::getUserId,GradeClassEntity::getPrincipalId)
                .eq(form.getSchoolId() != null, GradeClassEntity::getSchoolId, form.getSchoolId())
                .eq(form.getGradeId() != null, GradeClassEntity::getGradeId, form.getGradeId())
                .eq(SchoolEntity::getStatus,1)
                .gt(GradeClassEntity::getStatus,-1);
        IPage<GradeClassEntity> page = baseMapper.selectJoinPage(
                new Page<>(form.getPage(), form.getLimit()),
                GradeClassEntity.class,
                wrapper
        );

        return new PageUtils(page);
    }

}