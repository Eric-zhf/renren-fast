package com.exercise.modules.structure.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exercise.common.utils.PageUtils;
import com.exercise.modules.structure.entity.SchoolEntity;
import com.exercise.modules.structure.entity.form.SchoolForm;

import java.util.Map;

/**
 * 学校管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
public interface SchoolService extends IService<SchoolEntity> {

    PageUtils queryPage(SchoolForm form);
}

