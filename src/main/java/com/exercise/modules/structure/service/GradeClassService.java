package com.exercise.modules.structure.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exercise.common.utils.PageUtils;
import com.exercise.modules.structure.entity.GradeClassEntity;
import com.exercise.modules.structure.entity.form.GradeClassForm;

import java.util.Map;

/**
 * 班级管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
public interface GradeClassService extends IService<GradeClassEntity> {

    PageUtils queryPage(GradeClassForm form);
}

