package com.exercise.modules.structure.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exercise.modules.sys.entity.SysUserEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.github.yulichang.wrapper.MPJLambdaWrapper;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.Query;

import com.exercise.modules.structure.dao.SchoolDao;
import com.exercise.modules.structure.entity.SchoolEntity;
import com.exercise.modules.structure.entity.form.SchoolForm;
import com.exercise.modules.structure.service.SchoolService;


@Service("schoolService")
public class SchoolServiceImpl extends ServiceImpl<SchoolDao, SchoolEntity> implements SchoolService {

    @Override
    public PageUtils queryPage(SchoolForm form) {
        MPJLambdaWrapper<SchoolEntity> wrapper = new MPJLambdaWrapper<SchoolEntity>()
                .selectAll(SchoolEntity.class)
                .selectAs(SysUserEntity::getUsername, SchoolEntity::getPrincipalName)
                .leftJoin(SysUserEntity.class, SysUserEntity::getUserId, SchoolEntity::getPrincipalId)
                .like(StringUtils.isNotBlank(form.getSchoolName()), SchoolEntity::getSchoolName, form.getSchoolName())
                .gt(SchoolEntity::getStatus, -1);
        IPage<SchoolEntity> page = baseMapper.selectJoinPage(
                new Page<>(form.getPage(), form.getLimit()),
                SchoolEntity.class,
                wrapper
        );

        return new PageUtils(page);
    }

}