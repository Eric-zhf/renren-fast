package com.exercise.modules.structure.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.Query;

import com.exercise.modules.structure.dao.GradeDao;
import com.exercise.modules.structure.entity.GradeEntity;
import com.exercise.modules.structure.entity.form.GradeForm;
import com.exercise.modules.structure.service.GradeService;


@Service("gradeService")
public class GradeServiceImpl extends ServiceImpl<GradeDao, GradeEntity> implements GradeService {

    @Override
    public PageUtils queryPage(GradeForm form) {
        MPJLambdaWrapper<GradeEntity> wrapper = new MPJLambdaWrapper<GradeEntity>()
                .selectAll(GradeEntity.class);
        IPage<GradeEntity> page = baseMapper.selectJoinPage(
                new Page<>(form.getPage(),form.getLimit()),
            GradeEntity.class,
                wrapper
        );

        return new PageUtils(page);
    }

}