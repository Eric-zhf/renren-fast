package com.exercise.modules.structure.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.exercise.common.utils.CharUtils;
import com.exercise.common.utils.PageVO;
import com.exercise.modules.exam.entity.ExerciseEntity;
import com.exercise.modules.sys.controller.AbstractController;
import com.exercise.modules.sys.entity.SysUserEntity;
import com.exercise.modules.sys.service.SysUserService;
import com.github.pagehelper.PageHelper;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.exercise.modules.structure.entity.GradeClassEntity;
import com.exercise.modules.structure.entity.form.GradeClassForm;
import com.exercise.modules.structure.service.GradeClassService;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.R;

import javax.servlet.http.HttpServletRequest;


/**
 * 班级管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
@RestController
@RequestMapping("structure/gradeclass")
public class GradeClassController extends AbstractController {
    @Autowired
    private GradeClassService gradeClassService;
    private final SysUserService sysUserService;

    public GradeClassController(SysUserService sysUserService) {
        this.sysUserService = sysUserService;
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("structure:gradeclass:list")
    public R list(GradeClassForm form) {
        PageUtils page = gradeClassService.queryPage(form);

        return R.ok().put("page", page);
    }

    @GetMapping("/listAll")
    public R listAll(Integer schoolId, Integer gradeId) {
        List<GradeClassEntity> list = gradeClassService.list(new MPJLambdaWrapper<GradeClassEntity>().eq(schoolId != null, GradeClassEntity::getSchoolId, schoolId).eq(gradeId != null, GradeClassEntity::getGradeId, gradeId).eq(GradeClassEntity::getStatus, 1));
        return R.ok().put("data", list);
    }

    @RequestMapping("/listUser")
    public R listUser(Integer classId, Integer page, Integer limit) {
        Long userId = getUserId();
        GradeClassEntity gradeClass = gradeClassService.getOne(new MPJLambdaWrapper<GradeClassEntity>().eq(GradeClassEntity::getPrincipalId, userId));
        if (gradeClass != null) {
            classId = gradeClass.getClassId();
        }
        PageHelper.startPage(page, limit);
        List<SysUserEntity> list = sysUserService.list(new MPJLambdaWrapper<SysUserEntity>().eq(SysUserEntity::getClassId, classId).eq(SysUserEntity::getStatus, 1));
        return R.ok().put("data", new PageVO<>(list));
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{classId}")
    @RequiresPermissions("structure:gradeclass:info")
    public R info(@PathVariable("classId") Integer classId) {
        GradeClassEntity gradeClass = gradeClassService.getById(classId);
        if (gradeClass!=null){
            SysUserEntity byId = sysUserService.getById(gradeClass.getPrincipalId());
            if (byId!=null){
                gradeClass.setPrincipalName(byId.getUsername());
            }
        }
        return R.ok().put("gradeClass", gradeClass);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("structure:gradeclass:save")
    public R save(@RequestBody GradeClassEntity gradeClass) {
        String className = gradeClass.getClassName();
        String principalName = gradeClass.getPrincipalName();
        SysUserEntity user = new SysUserEntity();
        SysUserEntity one = sysUserService.getOne(new MPJLambdaWrapper<SysUserEntity>().eq(SysUserEntity::getUsername, principalName));
        if (one!=null){
            return R.error("账号重复，请更换账号后重试");
        }
        user.setUsername(principalName);
        user.setRealName(className);
        user.setPassword(CharUtils.getPinyin(principalName));
        user.setCreateUserId(1L);
        List<Long> list = new ArrayList<>();
        list.add(2L);
        user.setRoleIdList(list);
        sysUserService.saveUser(user);
        gradeClass.setPrincipalId(Integer.parseInt(user.getUserId().toString()));
        gradeClassService.save(gradeClass);
        Integer principalId = gradeClass.getPrincipalId();
        if (principalId != null) {
            SysUserEntity updateUser = new SysUserEntity();
            updateUser.setUserId(Long.parseLong(principalId.toString()));
            updateUser.setClassId(gradeClass.getClassId());
            updateUser.setGradeId(gradeClass.getGradeId());
            updateUser.setSchoolId(gradeClass.getSchoolId());
            sysUserService.updateById(updateUser);
        }

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("structure:gradeclass:update")
    public R update(@RequestBody GradeClassEntity gradeClass) {
        String principalName = gradeClass.getPrincipalName();
        Integer status = gradeClass.getStatus();
        SysUserEntity user = new SysUserEntity();
        user.setUserId(Long.parseLong(gradeClass.getPrincipalId().toString()));
        if (StringUtils.isNotBlank(principalName)){
            user.setUsername(principalName);
        }
        user.setStatus(status);
        sysUserService.updateById(user);
        gradeClassService.updateById(gradeClass);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("structure:gradeclass:delete")
    public R delete(@RequestBody Integer[] classIds) {
        List<GradeClassEntity> list = new ArrayList<>();
        for (Integer classId : classIds) {
            GradeClassEntity exercise = new GradeClassEntity();
            exercise.setClassId(classId);
            exercise.setStatus(-1);
            list.add(exercise);
        }
        gradeClassService.updateBatchById(list);

        return R.ok();
    }

}
