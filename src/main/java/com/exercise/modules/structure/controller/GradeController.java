package com.exercise.modules.structure.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.github.yulichang.wrapper.MPJLambdaWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exercise.modules.structure.entity.GradeEntity;
import com.exercise.modules.structure.entity.form.GradeForm;
import com.exercise.modules.structure.service.GradeService;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.R;



/**
 * 年级管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
@RestController
@RequestMapping("structure/grade")
@Api("年级列表")
public class GradeController {
    @Autowired
    private GradeService gradeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("structure:grade:list")
    public R list(GradeForm form){
        PageUtils page = gradeService.queryPage(form);

        return R.ok().put("page", page);
    }

    @ApiOperation("查询所有年级")
    @RequestMapping("/listAll")
    public R listAll(){
        List<GradeEntity> list = gradeService.list();
        return R.ok().put("data",list);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{gradeId}")
    @RequiresPermissions("structure:grade:info")
    public R info(@PathVariable("gradeId") Integer gradeId){
		GradeEntity grade = gradeService.getById(gradeId);

        return R.ok().put("grade", grade);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("structure:grade:save")
    public R save(@RequestBody GradeEntity grade){
		gradeService.save(grade);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("structure:grade:update")
    public R update(@RequestBody GradeEntity grade){
		gradeService.updateById(grade);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("structure:grade:delete")
    public R delete(@RequestBody Integer[] gradeIds){
		gradeService.removeByIds(Arrays.asList(gradeIds));

        return R.ok();
    }

}
