package com.exercise.modules.structure.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.exercise.common.utils.CharUtils;
import com.exercise.modules.structure.entity.GradeClassEntity;
import com.exercise.modules.sys.dao.SysUserDao;
import com.exercise.modules.sys.entity.SysUserEntity;
import com.exercise.modules.sys.service.SysUserService;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.exercise.modules.structure.entity.SchoolEntity;
import com.exercise.modules.structure.entity.form.SchoolForm;
import com.exercise.modules.structure.service.SchoolService;
import com.exercise.common.utils.PageUtils;
import com.exercise.common.utils.R;


/**
 * 学校管理
 *
 * @author Eric
 * @email xuzhen0xz@163.com
 * @date 2024-01-29 17:18:04
 */
@RestController
@RequestMapping("structure/school")
public class SchoolController {
    @Autowired
    private SchoolService schoolService;
    private final SysUserService sysUserService;

    public SchoolController(SysUserService sysUserService) {
        this.sysUserService = sysUserService;
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("structure:school:list")
    public R list(SchoolForm form) {
        PageUtils page = schoolService.queryPage(form);

        return R.ok().put("page", page);
    }

    @GetMapping("/listAll")
    public R listAll() {
        return R.ok().put("data", schoolService.list(new MPJLambdaWrapper<SchoolEntity>().eq(SchoolEntity::getStatus, 1)));
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{schoolId}")
    @RequiresPermissions("structure:school:info")
    public R info(@PathVariable("schoolId") Integer schoolId) {
        SchoolEntity school = schoolService.getById(schoolId);
        if (school != null) {
            Integer principalId = school.getPrincipalId();
            SysUserEntity byId = sysUserService.getById(principalId);
            school.setPrincipalName(byId.getUsername());
        }
        return R.ok().put("school", school);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("structure:school:save")
    public R save(@RequestBody SchoolEntity school) {
        String schoolName = school.getSchoolName();
        SchoolEntity oneSchool = schoolService.getOne(new MPJLambdaWrapper<SchoolEntity>().eq(SchoolEntity::getSchoolName, schoolName));
        if (oneSchool!=null){
            return R.error("已存在此学校");
        }
        SysUserEntity user = new SysUserEntity();
        String principalName = school.getPrincipalName();
        user.setUsername(principalName);
        SysUserEntity one = sysUserService.getOne(new MPJLambdaWrapper<SysUserEntity>().eq(SysUserEntity::getUsername, principalName));
        if (one!=null){
            return R.error("账号重复，请更换账号后重试");
        }
        user.setRealName(schoolName);
        user.setPassword(CharUtils.getPinyin(schoolName));
        user.setCreateUserId(1L);
        List<Long> list = new ArrayList<>();
        list.add(1L);
        user.setRoleIdList(list);
        sysUserService.saveUser(user);
        school.setPrincipalId(Integer.parseInt(user.getUserId().toString()));
        schoolService.save(school);
        Integer principalId = school.getPrincipalId();
        if (principalId != null) {
            SysUserEntity updateUser = new SysUserEntity();
            updateUser.setUserId(user.getUserId());
            updateUser.setSchoolId(school.getSchoolId());
            sysUserService.updateById(updateUser);
        }

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("structure:school:update")
    public R update(@RequestBody SchoolEntity school) {
        String principalName = school.getPrincipalName();
        Integer status = school.getStatus();
        SysUserEntity user = new SysUserEntity();
        user.setUserId(Long.parseLong(school.getPrincipalId().toString()));
        if (StringUtils.isNotBlank(principalName)){
            user.setUsername(principalName);
        }
        user.setStatus(status);
        sysUserService.updateById(user);
        schoolService.updateById(school);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("structure:school:delete")
    public R delete(@RequestBody Integer[] schoolIds) {
        List<SchoolEntity> list = new ArrayList<>();
        for (Integer schoolId: schoolIds) {
            SchoolEntity exercise = new SchoolEntity();
            exercise.setSchoolId(schoolId);
            exercise.setStatus(-1);
            list.add(exercise);
        }
        schoolService.updateBatchById(list);

        return R.ok();
    }

    public static void main(String[] args) {
        System.err.println(CharUtils.getPinyin("测试学校2",""));
    }

}
