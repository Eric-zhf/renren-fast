package com.exercise.common.utils;

/**
 *
 * @author Administrator
 * @date 2017/11/30
 */
public class PageInfo {
   private int index;
   private int limit=20;
   private int page=1;
   public int getIndex(){
      return (page-1)*limit;
   }

   public void setIndex(int index) {
      this.index = index;
   }

   public int getLimit() {
      return limit;
   }

   public void setLimit(int pageSize) {
      this.limit = pageSize;
   }

   public int getPage() {
      return page;
   }

   public void setPage(int page) {
      this.page = page;
   }
}
