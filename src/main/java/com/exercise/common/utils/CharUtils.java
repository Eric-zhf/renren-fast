package com.exercise.common.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.apache.commons.lang.StringUtils;

import java.util.Random;

/**
 * @ClassName: CharUtils
 * @Description:
 * @Author: raindrops
 * @Date: 2024/2/23 22:03
 */
public class CharUtils {

    public static String getChineseFirstLetters(String chineseText) {
        StringBuilder result = new StringBuilder();

        for (char c : chineseText.toCharArray()) {
            if (Character.toString(c).matches("[\\u4E00-\\u9FA5]+")) {
                // 如果是中文字符，提取拼音首字母
                String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(c);
                if (pinyinArray != null && pinyinArray.length > 0) {
                    result.append(pinyinArray[0].charAt(0));
                }
            } else {
                // 如果是非中文字符，直接添加
                result.append(c);
            }
        }
        return result.toString().toLowerCase();
    }

    public static String convertToPinyin(String chineseText) {
        StringBuilder pinyinBuilder = new StringBuilder();

        for (char c : chineseText.toCharArray()) {
            if (Character.toString(c).matches("[\\u4E00-\\u9FA5]+")) {
                // 如果是中文字符，获取拼音
                String[] pinyinArray = PinyinHelper.toTongyongPinyinStringArray(c);
                if (pinyinArray != null && pinyinArray.length > 0) {
                    // 取第一个拼音
                    pinyinBuilder.append(pinyinArray[0]);
                    pinyinBuilder.append(removeTone(pinyinArray[0]));
                } else {
                    // 如果无法获取拼音，直接添加原字符
                    pinyinBuilder.append(c);
                }
            } else {
                // 如果是非中文字符，直接添加
                pinyinBuilder.append(c);
            }
        }

        return pinyinBuilder.toString();
    }
    private static String removeTone(String pinyin) {
        // 去除声调
        return pinyin.replaceAll("[\\d]", "");
    }

    public static String getPinyin(String text) {
        return getPinyin(text,"");
    }
    /**
     * 将汉字转换为全拼
     *
     * @param text 文本
     * @param separator 分隔符
     * @return {@link String}
     */
    public static String getPinyin(String text, String separator) {
        char[] chars = text.toCharArray();
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        // 设置大小写
        format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        // 设置声调表示方法
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        // 设置字母u表示方法
        format.setVCharType(HanyuPinyinVCharType.WITH_V);
        String[] s;
        String rs = StringUtils.EMPTY;
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < chars.length; i++) {
                // 判断是否为汉字字符
                if (String.valueOf(chars[i]).matches("[\\u4E00-\\u9FA5]+")) {
                    s = PinyinHelper.toHanyuPinyinStringArray(chars[i], format);
                    if (s != null) {
                        sb.append(s[0]).append(separator);
                        continue;
                    }
                }
                System.err.println(chars[i]);
                sb.append(chars[i]);
                if ((i + 1 >= chars.length) || String.valueOf(chars[i + 1]).matches("[\\u4E00-\\u9FA5]+")) {
                    sb.append(separator);
                }
            }
            rs = sb.substring(0, sb.length());
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            e.printStackTrace();
        }
        return rs;
    }


    public static String generateRandomString(int length) {
        // 定义包含可能字符的字符数组
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        char[] charArray = characters.toCharArray();

        // 使用Random类生成随机字符
        Random random = new Random();
        StringBuilder randomString = new StringBuilder();

        // 生成指定长度的随机字符串
        for (int i = 0; i < length; i++) {
            char randomChar = charArray[random.nextInt(charArray.length)];
            randomString.append(randomChar);
        }

        return randomString.toString();
    }
}
