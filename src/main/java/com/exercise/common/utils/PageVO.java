package com.exercise.common.utils;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import com.github.pagehelper.PageInfo;

/**
 * @ClassName: PageVO
 * @Description:
 * @Author: raindrops
 * @Date: 2022/2/16 9:59 上午
 */
@Data
public class PageVO<T> implements Serializable {
    /** list数据 */
    private List<T> list;
    /** 总条数 */
    private Long totalCount;
    /** 当前页码 */
    private Integer currPage;
    /** 总页数 */
    private Integer totalPage;
    /** 每页条数 */
    private Integer pageSize;
    /** 当页条数 */
    private Integer currentNumber;
    {
        if (this.pageSize==null || this.pageSize==0){
            this.pageSize = 20;
        }
    }

    public PageVO() {}

    public PageVO(List<T> list) {
        PageInfo<T> pageInfo = new PageInfo<>(list);
        this.setPageSize(pageInfo.getPageSize());
        this.setList(pageInfo.getList());
        this.setCurrPage(pageInfo.getPageNum());
        this.setTotalCount(pageInfo.getTotal());
        this.setTotalPage(pageInfo.getPages());
        this.setCurrentNumber(pageInfo.getSize());
        this.setPageSize(pageInfo.getPageSize());
    }

    public PageVO<T> format(PageInfo<T> pageInfo){
        this.setPageSize(pageInfo.getPageSize());
        this.setList(pageInfo.getList());
        this.setCurrPage(pageInfo.getPageNum());
        this.setTotalCount(pageInfo.getTotal());
        this.setTotalPage(pageInfo.getPages());
        this.setCurrentNumber(pageInfo.getSize());
        this.setPageSize(pageInfo.getPageSize());
        return this;
    }


    public void init(long total, int currentPage, List<T> list) {
        this.currPage = currentPage;
        this.list = list;
        this.totalCount = total;
        this.totalPage = Math.toIntExact(total / pageSize);
        this.currentNumber = list.size();
        if (total / pageSize>1 && total % pageSize > 0) {
            this.totalPage += 1;
        }
    }


    public void init(long total, int currentPage,int totalPage, List<T> list) {
        this.currPage = currentPage;
        this.list = list;
        this.totalCount = total;
        this.totalPage = totalPage;
        this.currentNumber = list.size();
        if (total / pageSize>1 && total % pageSize > 0) {
            this.totalPage += 1;
        }
    }


}
