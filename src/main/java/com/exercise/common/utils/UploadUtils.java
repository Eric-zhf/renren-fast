package com.exercise.common.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * @ClassName: UploadUtils
 * @Description:
 * @Author: raindrops
 * @Date: 2024/2/24 17:30
 */
@RestController
@RequestMapping("/api")
public class UploadUtils {

    @Value("${file.path}")
    private String filePath;
    @Value("${file.urlSuffix}")
    private String fileUrlSuffix;


    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public R upload(MultipartFile file) throws IOException {
        String time = DateUtils.format(new Date(),DateUtils.DATE_PATTERN_2);
        String path = filePath + time;
        String originalFilename = file.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        suffix = suffix.toLowerCase();
        String fileName = UUID.randomUUID() + suffix;
        File targetFile = new File(path, fileName);
        if (!targetFile.getParentFile().exists()) {
            //注意，判断父级路径是否存在
            targetFile.getParentFile().mkdirs();
        }
        String fileUrl = fileUrlSuffix+time+"/"+fileName;
        //保存
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("上传失败");
        }
        return R.ok().put("data",fileUrl);
    }
}
